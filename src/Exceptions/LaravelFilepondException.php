<?php

namespace Superius\Filepond\Exceptions;

use Throwable;

interface LaravelFilepondException extends Throwable
{
}
